import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useHistory
} from 'react-router-dom'
import Login from '../src/pages/Login/Login'
import Users from './pages/CreateUsers/Users'

const Routes = () => {

    const history = useHistory();

    return (
        <Router >
            <Switch history={history}>
                <Route exact path='/' component={Login}/>
                <Route exact path='/Users' component={Users}/>
            </Switch>
        </Router>
    )
};

export default Routes;