import firebase from 'firebase';
import 'firebase/firestore';

const config = {
    apiKey: "AIzaSyCkzAIna0pM0-xuHJ_8iQ28xAm1UFNDFWE",
    authDomain: "login-dc13c.firebaseapp.com",
    databaseURL: "https://login-dc13c.firebaseio.com",
    projectId: "login-dc13c",
    storageBucket: "login-dc13c.appspot.com",
    messagingSenderId: "981024744409",
}

if(!firebase.apps.length){
    firebase.initializeApp(config);
}

export const db = firebase.firestore();
export const auth = firebase.auth();
export default firebase;
