import React, { useState } from "react";
import "./Login.css";
import Title from "./components/Title/Title";
import Label from "./components/Label/Label";
import Input from "./components/Input/Input";
import Home from '../Home/Home';
import {auth} from '../../firebase/FirebaseConfig';



const Login = ({history}) => {
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [login, setLogin] = useState(false);
  const [hasError, setHasError] = useState(false);

  function handleChange(name, value) {
    if (name === "usuario") {
      setUser(value);
      setHasError(false);
    } else {
      if (value.length < 6) {
        setError(true);
        setHasError(false);
      } else {
        setError(false);
        setPassword(value);
        setHasError(false);
      }
    }
  }
  function ifMatch(param) {
    if (param.user.length > 0 && param.password.length > 0) {
      if (param.user === "usuario" && param.password === "contraseña") {
        const { user, password } = param;
        let ac = { user, password };
        let account = JSON.stringify(ac);
        localStorage.setItem("account", account);
        setLogin(true);
      } else {
        setLogin(false);
        setHasError(true);
      }
    } else {
      setLogin(false);
      setHasError(true);
    }
  }

  function handleSubmit() {
    let account = { user, password };
    if (account) {
      ifMatch(account);
    }
  }

  function handleonCLick() {
    history.push('./users')
  }

  return (
    <div className="login-container">
      {login ? (
        <div className="home-container">
          <h1>¡Hola, {user}! </h1>
          <label>Felicitaciones, estas logueado!</label>
        </div>
      ) : (
        <div className="login-content">
          <Title text="¡Bienvenido!" />
          {hasError && (
            <label className="label-alert">Su contraseña es incorrecta</label>
          )}
          <Label text="Usuario" />
          <Input
            attribute={{
              id: "usuario",
              name: "usuario",
              type: "text",
              placeholder: "Ingrese su Usuario",
            }}
            handleChange={handleChange}
          />
          <Label text="Contraseña" />
          <Input
            attribute={{
              id: "contraseña",
              name: "contraseña",
              type: "password",
              placeholder: "Ingrese su Contraseña",
            }}
            handleChange={handleChange}
            param={error}
          />
          {error && (
            <label className="label-error">
              Contraseña invalida o incompleta
            </label>
          )}
          <div className="submit-button-container">
            <button onClick={handleSubmit} className="submit-button">
              Ingresar
            </button>
          </div>

          <div onClick={handleonCLick} className="submit-link-container">
            <label className='link-container'>
            <Home history={history}/>
            </label>
           
          </div>
        </div>

        

      )}
    </div>
  );
};

export default Login;
