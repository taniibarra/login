import React, {useState} from 'react'
import './CreateUsers.css'
import Input from '../../../Login/components/Input/Input'


const CreateUsers = () => {

    const [password, setPassword] = useState('');
    const [error, setError] = useState(false);

    function handleChange(pass, value) {
        if(pass > 6) {
            setPassword(value)
            setError(false);
        } else {
            if (value.length < 6) {
                setError(true)
                setPassword(value)
            } else {
                setError(false)
                setPassword(value)
            }
        }
    }
    

    return (
        <div className="input-container-user">
            <div className='input-content-user'>
                <label> Nombre
                    <Input 
                    attribute={{
                        id: "nombre",
                        name: "nombre",
                        type: "text",
                        placeholder: "Ingrese su Nombre",
                      }}
                    
                    className='regular-style-user'/>
                </label>
                <label> Apellido
                    <Input 
                    attribute={{
                        id: "apellido",
                        name: "apellido",
                        type: "text",
                        placeholder: "Ingrese su Apellido",
                      }}
                    className='regular-style-user'/>
                </label>    
                <label> Usuario
                    <Input 
                    attribute={{
                        id: "usuario",
                        name: "usuario",
                        type: "text",
                        placeholder: "Ingrese su Usuario",
                      }}
                    className='regular-style-user'/>
                </label> 
                <label> Contraseña
                    <Input 
                    attribute={{
                        id: "contraseña",
                        name: "contraseña",
                        type: "password",
                        placeholder: "Ingrese su Contraseña",
                      }}
                      handleChange={handleChange}
                      setPassword={setPassword}
                      className='regular-style-user'/>
                    {error && (
                        <label className="label-error">
                        La contraseña debe contener seis carácteres
                        </label>
                    )}
                </label>
                <label> Confirmar Contraseña
                    <Input 
                    attribute={{
                        id: "contraseña",
                        name: "contraseña",
                        type: "password",
                        placeholder: "Repita su Contraseña",
                      }}
                      password={password}
                      handleChange={handleChange}
                      className='regular-style-user'/>
                   
                </label>
                
                <button className='btn-user'>Crear Cuenta</button>
            </div>
        </div>

       

    )
};

export default CreateUsers;